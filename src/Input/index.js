import React from "react";

import "./style.css";

const Input = ({
                 children,
                 placeholder,
                 color = "black",
                 ...props
               }) => (
  <input
    placeholder={placeholder}
    className={`Input Input_${color}`} {...props}>
    {children}
  </input>
);

export default Input;
