import React from "react";
import Input from "../Input";
import Button from "../Button";
import Link from "../Link";

import "./style.css";

const Login = () => (
  <div className="Login">
    <span className="Login-text">Please enter your email and password then press Login</span>{" "}
    <LoginForm/>
  </div>
);

class LoginForm extends React.Component {
  state = {
    value: this.props.organizationName
  };
  
  onChange = event => {
    this.setState({ value: event.target.value });
  };
  
  onSubmit = event => {
    this.props.onOrganizationSearch(this.state.value);
    
    event.preventDefault();
  };
  
  render() {
    const { value } = this.state;
    
    return (
      <div className="Navigation-login">
        <form className="Login-form" onSubmit={this.onSubmit}>
          <Input
            placeholder={"Input username"}
            color={"login"}
            type="text"
            value={value}
            onChange={this.onChange}/>
          <br className="Login-spacing"/>
          <br className="Login-spacing"/>
          <Input
            placeholder={"Input password"}
            color={"login"}
            type="text"
            value={value}
            onChange={this.onChange}/>
          <br/>
          <br/>
          <Button color={"login"} type="submit">
            Login ...
          </Button>
        </form>
      </div>
    );
  }
}

export default Login;